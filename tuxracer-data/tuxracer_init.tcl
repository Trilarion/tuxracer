# Tux Racer initialization script

#
# Set Up Music
#

# Splash & Start screens
if { [tux_load_music start_screen music/start1-jt.it] } {
    tux_bind_music splash_screen start_screen -1
    tux_bind_music start_screen start_screen -1
}

# Options screen
if { [tux_load_music options_screen music/options1-jt.it] } {
    # not used currently
}

# Music played during race
if { [tux_load_music racing music/race1-jt.it] } {
    tux_bind_music intro racing -1
    tux_bind_music racing racing -1
    tux_bind_music paused racing -1
}

# Game Over screen
if { [tux_load_music game_over music/wonrace1-jt.it] } {
    tux_bind_music game_over game_over 1
}


#
# Set Up Sounds
#

# Tree Hit
if { [tux_load_sound tree_hit1 sounds/tux_hit_tree1.wav] } {
    tux_bind_sounds tree_hit tree_hit1 
}

# Snow Sliding
if { [tux_load_sound snow_sound sounds/tux_on_snow1.wav] } {
    tux_bind_sounds snow_sound snow_sound
    tux_bind_sounds flying_sound snow_sound
}
# Rock Sliding
if { [tux_load_sound rock_sound sounds/tux_on_rock1.wav] } {
    tux_bind_sounds rock_sound rock_sound
}
# Ice Sliding
if { [tux_load_sound ice_sound sounds/tux_on_ice1.wav] } {
    tux_bind_sounds ice_sound ice_sound
}

# "Ow!"
if { [tux_load_sound ow1 sounds/ow1-jb.wav] &&
     [tux_load_sound ow2 sounds/ow2-jb.wav] &&
     [tux_load_sound ow3 sounds/ow3-jb.wav] &&
     [tux_load_sound ow4 sounds/ow4-jb.wav] } \
{
    tux_bind_sounds hard_collision ow1 ow2 ow2 ow4
}

# "Let's Go!"
if { [tux_load_sound letsgo1 sounds/letsgo1-jb.wav] &&
     [tux_load_sound letsgo2 sounds/letsgo2-jb.wav] } \
{
    tux_bind_sounds start_race letsgo1 letsgo2
}

# "Look out!"
if { [tux_load_sound lookout1 sounds/lookout1-jb.wav] &&
     [tux_load_sound lookout2 sounds/lookout2-jb.wav] } \
{
    tux_bind_sounds going_to_collide lookout1 lookout2
}

# Sky Cube
tux_load_texture alpine1-front courses/common/alpine1-front.rgb 0
tux_load_texture alpine1-right courses/common/alpine1-right.rgb 0
tux_load_texture alpine1-left courses/common/alpine1-left.rgb 0
tux_load_texture alpine1-back courses/common/alpine1-back.rgb 0
tux_load_texture alpine1-top courses/common/alpine1-top.rgb 0
tux_load_texture alpine1-bottom courses/common/alpine1-bottom.rgb 0
tux_load_texture alpine1-sphere courses/common/alpine1-sphere.rgb 0

tux_bind_texture sky_front alpine1-front
tux_bind_texture sky_right alpine1-right
tux_bind_texture sky_left alpine1-left
tux_bind_texture sky_back alpine1-back
tux_bind_texture sky_top alpine1-top
tux_bind_texture sky_bottom alpine1-bottom
tux_bind_texture terrain_envmap alpine1-sphere

# Track marks
tux_load_texture track_head courses/common/buttstart.rgb 1
tux_load_texture track_mark courses/common/buttprint.rgb 1
tux_load_texture track_tail courses/common/buttstop.rgb 1
tux_bind_texture track_head track_head
tux_bind_texture track_mark track_mark
tux_bind_texture track_tail track_tail

# Splash screen
tux_load_texture splash_screen_tr textures/splash_tr.rgb 0
tux_load_texture splash_screen_br textures/splash_br.rgb 0
tux_load_texture splash_screen_tl textures/splash_tl.rgb 0
tux_load_texture splash_screen_bl textures/splash_bl.rgb 0

tux_bind_texture splash_screen_tr splash_screen_tr
tux_bind_texture splash_screen_br splash_screen_br
tux_bind_texture splash_screen_tl splash_screen_tl
tux_bind_texture splash_screen_bl splash_screen_bl

# Snow Particle
tux_load_texture snow_particle courses/common/snowparticles.rgb 0
tux_bind_texture snow_particle snow_particle

# Fonts
tux_load_texture trebuchet_yel_blk fonts/trebuchet_yel_blk.rgb 0
tux_load_font -name trebuchet_yel_blk -file fonts/trebuchet_yel_blk.tfm \
              -texture trebuchet_yel_blk
tux_bind_font -binding time_label -font trebuchet_yel_blk -size 20
tux_bind_font -binding time_value -font trebuchet_yel_blk -size 30
tux_bind_font -binding time_hundredths -font trebuchet_yel_blk -size 20
tux_bind_font -binding herring_count -font trebuchet_yel_blk -size 30
tux_bind_font -binding speed_digits -font trebuchet_yel_blk -size 35
tux_bind_font -binding speed_units -font trebuchet_yel_blk -size 20
tux_bind_font -binding fps -font trebuchet_yel_blk -size 20

# HUD
tux_load_texture herring_icon textures/herringicon.rgb 0
tux_bind_texture herring_icon herring_icon

tux_load_texture gauge_outline textures/gaugeoutline.rgb 0
tux_bind_texture gauge_outline gauge_outline

tux_load_texture gauge_energy_mask textures/gaugeenergymask.rgb 0
tux_bind_texture gauge_energy_mask gauge_energy_mask

tux_load_texture gauge_speed_mask textures/gaugespeedmask.rgb 0
tux_bind_texture gauge_speed_mask gauge_speed_mask

# UI Widgets
tux_load_texture listbox_arrows textures/listbox_arrows.rgb 0
tux_bind_texture listbox_arrows listbox_arrows

#
# Useful subroutines
#
proc tux_goto_data_dir {} {
    cd [ tux_get_param "data_dir" ]
}
