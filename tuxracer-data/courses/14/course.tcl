tux_course_name "David Fries dfries@mail.win.org"
tux_course_dim 100 600
tux_start_pt 50 3.5
tux_angle 30
tux_elev_scale 10
tux_elev elev.rgb
tux_terrain terrain.rgb
tux_tree_size 1.4 3.15
tux_trees trees.rgb
tux_bgnd_img ../common/background3.rgb
tux_tree_tex ../common/tree.rgb
tux_ice_tex ../common/ice.rgb
tux_rock_tex ../common/rock.rgb
tux_snow_tex ../common/snow.rgb
tux_friction 0.25 0.9 0.35

# Keyframe data
source ../common/tux_walk.tcl

source ../common/standard_light.tcl

# Tree shape
source ../common/tree_polyhedron.tcl
