#
# Course configuration
#
tux_course_name "Test 1"
tux_course_dim 17.5 175        ;# width, length of course in m
tux_start_pt 9.8 3.5           ;# start position, measured from left rear corner
tux_angle 23                   ;# angle of course
tux_elev_scale 7.0             ;# amount by which to scale elevation data
tux_elev elev.rgb              ;# bitmap specifying course elevations
tux_terrain terrain.rgb        ;# bitmap specifying terrains type
tux_tree_size 1.4 3.2          ;# average diameter, height of trees
tux_trees trees.rgb            ;# bitmap specifying tree locations

#
# Textures
#
tux_bgnd_img ../common/background1.rgb ;# image to use for background
tux_tree_tex ../common/tree.rgb   ;# tree image
tux_ice_tex ../common/ice.rgb     ;# ice image
tux_rock_tex ../common/rock.rgb   ;# rock image
tux_snow_tex ../common/snow.rgb   ;# snow image

tux_friction 0.25 0.9 0.35     ;# ice, rock, snow friction coefficients

#
# Introductory animation keyframe data
#
source ../common/tux_walk.tcl

#
# Lighting
#
source ../common/standard_light.tcl

#
# Fog
# These settings correspond to the default fog settings
# Note that in exp or exp2 mode, -start and -end have no effect 
# (they are used in linear mode; see course #2)
#
tux_fog -on -mode linear -density 0.005 -colour { 1 1 1 1 } -start 0 \
    -end [tux_get_param forward_clip_distance]

#
# Particle colour
#
tux_particle_colour { 1.0 1.0 1.0 1.0 }

#
# Tree shape
#
source ../common/tree_polyhedron.tcl

