tux_course_name "Test 3"
tux_course_dim 5.6 600
tux_start_pt 2.8 3.5
tux_angle 30
tux_elev_scale 1.75
tux_elev elev.rgb
tux_terrain terrain.rgb
tux_tree_size 1.4 3.15
tux_trees trees.rgb
tux_bgnd_img ../common/background3.rgb
tux_tree_tex ../common/tree.rgb
tux_ice_tex ../common/ice.rgb
tux_rock_tex ../common/rock.rgb
tux_snow_tex ../common/snow.rgb
tux_friction 0.25 0.9 0.35

# Keyframe data
source ../common/tux_walk.tcl

source ../common/standard_light.tcl

# Tree shape
source ../common/tree_polyhedron.tcl

# Particle colour
tux_particle_colour { 1.0 1.0 1.0 1.0 }
